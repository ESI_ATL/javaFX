package esi.atl.fx2.shape;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

public class PaneFxCircleHandler extends Application{

    private final static int PANE_WIDTH = 300;
    private final static int PANE_HEIGHT = 200;
    
    public static void main(String[] args) {
        launch(args);
    }
    
    public Circle createFxCircle(double centerX, double centerY,
                                        double radius, Color color) {
        Circle c = new Circle(centerX, centerY, radius);
        c.setFill(color);
        c.setOnMousePressed(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {            
                System.out.println("MousePressed : (" + event.getSceneX() + ", " +
                                   event.getSceneY() + ")");
            }
        });
        
        c.setOnDragDetected(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {                
                System.out.println("DragDetected : (" + event.getSceneX() + ", " +
                                   event.getSceneY() + ")");
            }
        });        
        
        Tooltip tp = new Tooltip("Circle " + centerX + " " + centerY + " " +
                                 radius);
        Tooltip.install(c, tp);

        return c;
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Pane circlePane = new Pane();
        circlePane.setPadding(new Insets(15.0));
        circlePane.getChildren().add(createFxCircle(PANE_WIDTH/2, PANE_HEIGHT/2, PANE_WIDTH/4, Color.PERU));
        
        Scene scene = new Scene(circlePane, PANE_WIDTH, PANE_HEIGHT);
        
        primaryStage.setTitle("Drawing");
        primaryStage.setScene(scene);
        primaryStage.show();        
    }

}
